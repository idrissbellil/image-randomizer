# Dataset simulation project

Simulate an object detection dataset from previously annotated images, or 
from classification single target images.

# Usage
```bash
usage: main.py [-h] [--override OVERRIDE] [--annotationpath ANNOTATIONPATH]
               [--count COUNT] [--minimgs MINIMGS] [--shape SHAPE SHAPE]
               [--outpath OUTPATH] [--imagespath IMAGESPATH]

Simulate an Object Detection dataset.

optional arguments:
  -h, --help            show this help message and exit
  --override OVERRIDE   re-create the dataset or just regenerate the tfrecords
                        from the existing data (default: True)
  --annotationpath ANNOTATIONPATH
                        The input annotation file
  --count COUNT         total number of the created images (default: 5)
  --minimgs MINIMGS     minimum number of target images to be considered
                        (default: 5)
  --shape SHAPE SHAPE   the shape of the output image (default: 600x1000)
  --outpath OUTPATH     The output path in the system in which all output
                        files are going to be created
  --imagespath IMAGESPATH
                        The image collection path on the system.
```
