#!/usr/bin/env python
# coding: utf-8

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from glob import glob
import xml.etree.ElementTree as ET
import tensorflow as tf
from object_detection.utils import dataset_util
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.font_manager import FontProperties
import matplotlib as mpl
import codecs
import hashlib
import json
import io
import os
import PIL.Image

class AnnotationsToTFRecords():
  """Process Annotations, create tfrecords, show statistics
"""

  def __init__(self, all_targets):
    """Save the the annotatios structure and extract the\
       required paramers.
"""
    self.target_names, self.num_imgs = \
          self.get_names_numbers(all_targets)
    self.labels, self.values = zip(*Counter(self.target_names).items())
    self.all_targets = all_targets
    self.dictionary = None


  def get_names_numbers(self, annotations):
      """Process the annotations raw structure and extract\
       the raw labels and the total objects number.
"""
      target_names = []
      num_imgs = 0
      for img in annotations:
          img_names = img['label']
          num_imgs += len(img_names)
          target_names.extend(img_names)

      return target_names, num_imgs


  def show_statistics(self, tfpath='', fn='statistics.png'):
      """"Plot and save the annotated objects vs object class statistics.
"""
      labels, values = self.labels, self.values
      indexes = np.arange(len(labels))

      plt.bar(indexes, values, 1)
      plt.xticks(indexes, labels, rotation=90) # fontproperties = zh_font)
      plt.savefig(os.path.join(tfpath, fn),  bbox_inches='tight')

  # # This portion creates the label map file
  def create_the_label_map(self, min_train=1, tfpath='',
          fn='auto_annotation_label_map.pbtxt'):
      """This file is required for the tensorflow object detction API.

 It is required to run this member function to compute the conversion\
and reverse dictionaries needed to create the tfrecords.
"""

      values, labels = self.values, self.labels
      saved_dict = dict()
      reverse_dict = dict()
      with codecs.open(os.path.join(tfpath, fn), 'w', encoding='utf8') as f:
          counter = 1
          for iii in range(len(labels)):
              if values[iii] < min_train :
                  continue
              saved_dict[labels[iii]] = counter
              reverse_dict[counter] = labels[iii]
              f.write(u'item {\n')
              f.write(u'\tid: {}\n'.format(counter))
              f.write(u"\tname: '{}'\n".format(labels[iii]))
              f.write(u'}\n\n')
              counter+=1
      self.dictionary, self.reverse_dict = saved_dict, reverse_dict


  # # Defining now the main conversion function

  def create_tf_example(self, example, saved_dict):
    """Process one tfrecord example at the time.

This portion was taken and adapted from the conversion scripts
available from the tensorflow object detection API.
"""
    # Process one image data per run
    height = int(example['height']) # Image height
    width = int(example['width']) # Image width
    filename = example['path'].encode('utf8')# Filename of the image.
    encoded_image_data = None # Encoded image bytes
    with tf.gfile.GFile(filename,'rb') as fid:
      encoded_jpg = fid.read()

    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = PIL.Image.open(encoded_jpg_io)
    key = hashlib.sha256(encoded_jpg).hexdigest()

    image_format = 'jpeg'.encode('utf8') # b'jpeg' or b'png'

    xmins = [] # List of normalized left x coordinates in bounding box (1 per box)
    xmaxs = [] # List of normalized right x coordinates in bounding box
               # (1 per box)
    ymins = [] # List of normalized top y coordinates in bounding box (1 per box)
    ymaxs = [] # List of normalized bottom y coordinates in bounding box
               # (1 per box)
    classes_text = [] # List of string class name of bounding box (1 per box)
    classes = [] # List of integer class id of bounding box (1 per box)

  # Loop oer the boxes and fill the above fields
  #   for box in example:

  #     box_name = ''
  #     for attr in box:
  #         if attr.attrib['name'] == u'brand牌':

    for i in range(len(example['label'])):
      ymins.append(float(example['tlx'][i]) / height)
      ymaxs.append(float(example['brx'][i]) / height)
      xmins.append(float(example['tly'][i]) / width)
      xmaxs.append(float(example['bry'][i]) / width)
      classes_text.append(example['label'][i].encode('utf8'))
      classes.append(saved_dict[example['label'][0]])

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


  def create_tfrecords(self, save_dir):
    """Loop over the annotations and create the tfrecords.

    Parameters:
    -----------
    save_dir: str
        The directory in which to save the tfrecords
"""

    assert self.dictionary is not None, \
      "Create the label map first using create_label_map()"
    annotations = self.all_targets
    saved_dict = self.dictionary
    num_iter = len(annotations)
    eval_num = num_iter // 10
    train_num = num_iter - eval_num
    print('train {}, evaluate {}'.format(train_num, eval_num))

    writer_train = tf.python_io.TFRecordWriter(os.path.join(save_dir, 'ZWZTech_train'))
    writer_eval = tf.python_io.TFRecordWriter(os.path.join(save_dir, 'ZWZTech_eval'))

    for counter,example in enumerate(annotations):
      tf_example = self.create_tf_example(example, saved_dict)
      if(counter < train_num):
          print('>>>> TRAINING')
          writer_train.write(tf_example.SerializeToString())
      else :
          print('>>>> EVALUATING')
          writer_eval.write(tf_example.SerializeToString())

    writer_train.close()
    writer_eval.close()

  # # test some random boxes #

  def draw_rect(self, img, bbox, value=1):
      """Draw a rectangle defined by bbox in img.
"""
      newimg = img.copy()
      x1, y1, x2, y2 = tuple(bbox.flatten())
      newimg[x1:x2,y1] =  1.0
      newimg[x2,y1:(y2+1)] = 1.0
      newimg[x1:x2,y2] = 1.0
      newimg[x1,y1:y2] = 1.0
      return newimg

  def test_random_boxes(self, annotations, n=30):
      """Draw random rectangles and show them.
"""
      test_indices = np.random.randint(0, len(annotations),n)
      for iii in test_indices:
          plt.figure()
          annotation = annotations[iii]
          img = plt.imread(annotation['path'])
          height = int(annotation['height'])
          width =  int(annotation['width'])
          newbox = np.array([
              int(annotation['tlx']),
              int(annotation['tly']),
              int(annotation['brx']),
              int(annotation['bry'])])
          newbox[newbox<0] = 0
          newbox = newbox * height / 54
          img = self.draw_rect(img, newbox)
          plt.imshow(img)
          plt.show()

