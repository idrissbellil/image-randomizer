#!/usr/bin/env python
# coding: utf-8

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import numpy as np
import cv2
from skimage import color
from matplotlib import pyplot as plt
from skimage.transform import resize
import json
from pathlib import Path
import os
from matplotlib.font_manager import FontProperties
import matplotlib as mpl
import argparse
import xml.etree.ElementTree as ET

from tfrecords_conversion import AnnotationsToTFRecords
from utils import DataRandomizer, cvat2json

def parse_args():
    """Argument parsing sepate routine.
"""
    parser = argparse.ArgumentParser(
            description='Create more samples of for an object detection dataset\
                    starting from a json-formated previously annotated dataset OR\
                    a CVAT XML data set as CVAT was previsouly used for annotation.')

    parser.add_argument('--override',
            type=lambda x: (str(x).lower() == 'true'), default=True,
            help='re-create the dataset or just regenerate the\
                    tfrecords from the existing data (default: True)')
    parser.add_argument('--annotationpath', type=str, default='',
            help='The input annotation file')
    parser.add_argument('--cvatxmlpath', type=str, default='',
            help='The input CVAT XML file')
    parser.add_argument('--attribclass', type=str, default='',
            help='If CVAT XML file input, provide this one to name which\
                    attribute to use as an object class otherwise ignore\
                    it or supply "" to use the main label name as the class')
    parser.add_argument('--count', type=int, default=5,
            help='total number of the created images (default: 5)')
    parser.add_argument('--minimgs', type=int, default=5,
            help='minimum number of target images to be\
                    considered (default: 5)')
    parser.add_argument('--shape', type=int, default=[600, 1000], nargs=2,
            help='the shape of the output image (default: 600x1000)')
    parser.add_argument('--outpath', type=str,
            help='The output path in the system in which all output files\
                    are going to be created')
    parser.add_argument('--imagespath', type=str,
            help='The image collection path on the system.')

    return parser.parse_args()

def randomize_annotations(
        override, n_output_imgs, shape, annotations,\
                out_path='/tmp/annotations/', in_imgs_path=''):
    """Randomize previously annotated image data.

    Parameters:
    -----------
    override: bool
        Whether to override the previsouly created final annotation file
    n_output_imgs: int
        How many images the randomizer needs to create
    shape: list
        The Shape of the output images. ex: (600, 600)
    annotations: list
        The previous annotations saved as a list of images which contain
        a list of boxes.
    out_path: str
        Provide the place where the output data is going to be stored.
        'out_annotaions.json', 'tfrecords' and 'imgs' are going to be
        created here.
    in_imgs_path: str
        Provide the place from where to read the input annotations and
        images.
"""

    out_height, out_width = shape

    # Check whether the output data directory exists
    data = Path(os.path.join(out_path, 'imgs'))
    if not data.exists():
        data.mkdir(parents=True)

    # read and store target regions we want the model to detect
    targets = []
    for a in annotations:
        img_path = os.path.join(in_imgs_path, a['path'])
        img = plt.imread(img_path)
        assert len(img.shape) == 2 or len(img.shape) == 3,\
                'Image shape should be (w x h) or (w x h x d)'
        if len(img.shape) == 2:
            height, width = img.shape
        else :
            height, width, _ = img.shape
        for box in a['boxes']:
            crop = img[box['tly']:box['bry'], box['tlx']:box['brx'], :]
            targets.append({'img': crop, 'label': box['label']})

    # Create output images and fill them with background portions
    # and targets
    all_targets = []
    out_annotation = Path(os.path.join(out_path,
        'out_annotations.json'))
    data_rand = DataRandomizer(targets, None, (out_height, out_width))

    # Override the output file if the user asked so or
    # the file doesn't exist
    if override or not out_annotation.exists():
        for i in range(n_output_imgs):
            img, out_targets = data_rand.create_simulated_img()
            img_path = os.path.join(out_path, 'imgs', '{}.jpg'.format(i))
            plt.imsave(img_path, img)
            out_targets['path'] = img_path
            all_targets.append(out_targets)

         # save the output annotations
        with open(os.path.join(out_path,
            'out_annotations.json'), 'w') as f:
                json.dump(all_targets, f, indent=4)

    # The file exists and the user didn't ask to override
    else:
        with open(os.path.join(out_path,
            'out_annotations.json'), 'r') as f:
                all_targets = json.load(f)

    # Draw n number of images to confirm the rectangles are in the right place
    #draw_rects(all_targets, n=3)

    # create the tfrecords
    # Check whether tfrecords directory exists, create it otherwise
    records_path = Path(os.path.join(out_path, 'tfrecords'))
    if not records_path.exists():
        records_path.mkdir(parents=True)

    tfrecord = AnnotationsToTFRecords(all_targets)
    tfrecord.show_statistics(tfpath=records_path)
    tfrecord.create_the_label_map(tfpath=records_path)
    tfrecord.create_tfrecords(records_path)

def draw_rects(targets, n=None):
    """Use OpenCV to draw rectangles and matplotlib to show
    randomly chosen images.

    Parameters:
    -----------
    targets: list
        List of targets we want to show case the rectangle positions
    n: int
        The number of targets we want to pick in order to draw the rectangles.
    """
    if n is not None:
        targets = np.random.choice(targets, size=n)
    for target in targets :
        image = cv2.imread(target['path'])
        for x1, y1, x2, y2 in zip(target['tlx'], target['tly'],
            target['brx'], target['bry']):
            image = cv2.rectangle(image,(y1,x1),(y2,x2),(0,0,255),2)

        plt.figure()
        plt.imshow(image)
        plt.show()

def main():
    args = parse_args()

    if args.cvatxmlpath == '':
        assert args.annotationpath != '',\
            "One path should be provided 'cvatxmlpath' or 'annotationpath'"
        # annotations should be something like
        # [{'path':'<path>', 'boxes':[box,...]} ...]
        with open(args.annotationpath) as f :
            annotations = json.load(f)
    else :
        xml_cvat = ET.parse(args.cvatxmlpath).getroot()
        annotations = cvat2json(xml_cvat, imgs_path=args.imagespath,
                attrib_as_class=args.attribclass)

    #with open('out_annotations.json', 'w') as f:
    #        json.dump(annotations, f, indent=2)

    randomize_annotations(args.override, args.count, args.shape, annotations,
            out_path=args.outpath, in_imgs_path=args.imagespath)

if __name__ == '__main__':
    main()

