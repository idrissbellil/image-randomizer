import numpy as np
from skimage.transform import resize
import imgaug.augmenters as iaa
import os

def cvat2json(cvat_xml, attrib_as_class='', imgs_path=''):
    """Convert CVAT XML data into json boxes randomization.
    """
    ret_json = []
    for img_cvat in cvat_xml.findall('image'):
        img_json = {
                'path': os.path.join(imgs_path,
                    img_cvat.attrib['name'].split('/')[-1])
                }
        img_json['boxes'] = []
        for box_cvat in img_cvat:
            box_json = {
                    'tlx': int(float(box_cvat.attrib['xtl'])),
                    'tly': int(float(box_cvat.attrib['ytl'])),
                    'brx': int(float(box_cvat.attrib['xbr'])),
                    'bry': int(float(box_cvat.attrib['ybr']))
                    }
            if attrib_as_class == "" :
                box_json['label'] = box_cvat.attrib['label'].lower()
            else :
                for attribute in box_cvat :
                    if attribute.attrib['name'] == attrib_as_class :
                        box_json['label'] = attribute.text.lower()
            img_json['boxes'].append(box_json)
        ret_json.append(img_json)
    return ret_json

class DataRandomizer():
  """Generate randomized object detection data images.
  """

  def __init__(self, targets, backgrounds, scene_shape):
    """Save the initial arguments of the randomizer

    Parameters:
    -----------
    targets: list
          List of the objects (images) to be inserted randomly.
    backgounds: list
          List of the images to be used as backgrounds in the randomizer.
    scene_shape: list
          The desired scene size. ex: (300, 300).
    """

    self.targets = targets
    self.backgrounds = backgrounds
    self.h, self.w = scene_shape

  def get_free_position(self, shape, positions, override=True, max_iter=300):
      """Try to get a free position.

      Parameters:
      -----------
      shape: numpy.ndarray
            The shape of the rectangle check a free position for.
      positions: list
            The list of positions the batch shouldn't intersect with.
      override: bool
            Whether it's allowed to intersect with other positions.
      max_iter: int
            The maximum iterations to find a location
      """

      for i in range(max_iter):
          #print('iteration {}'.format(i))
          x_tl = np.random.randint(0, self.h-shape[0])
          y_tl = np.random.randint(0, self.w-shape[1])
          x_br = x_tl + shape[0]
          y_br = y_tl + shape[1]
          if override or not self.batch_intersect(positions, [x_tl, y_tl, x_br, y_br]):
              return [x_tl, y_tl, x_br, y_br]
      return None

  def batch_intersect(self, batch_positions, batch):
      """Check if there are any intersections between batch (its position)
      and other batch positions.

      Parameters:
      -----------
      batch_positions: list
            previously inserted objects to check the intersection with.
      batch: list
            The batch rectangle we want to find place for. ex: [x1,y1,x2,y2].
      """

      for pos in batch_positions:
          if not (batch[1]>pos[3] or batch[0]>pos[2] or
                  batch[3]<pos[1] or batch[2]<pos[0]):
              return True
      return False

  def insert_batch_in_img(self, batch, img, pos):
      """
      Insert blindy btach into image (img) at the given position
      """
      b_h, b_w = batch.shape[0], batch.shape[1]
      mask = (batch==0)
      out_batch = batch.copy()
      area_to_replace = img[pos[0]:pos[0]+b_h, pos[1]:pos[1]+b_w]
      out_batch[mask] = area_to_replace[mask]
      img[pos[0]:pos[0]+b_h, pos[1]:pos[1]+b_w] = out_batch
      return img

  def create_simulated_img(self):
      """
      Insert a random number of targets into a random background image or
      white image
      """
      # Create output images and fill them with background areas
      # and targets

      # Specify the main background image
      if self.backgrounds is None:
        img = np.ones((self.h, self.w, 3), dtype=np.uint8) * 255
      else :
        img = np.random.choice(self.backgrounds)
        img = resize(img, (self.h, self.w, 3), anti_aliasing=True)

      # grab and insert a random number of targets
      n_targets = np.random.randint(1, 20)
      print('I will insert {} targets in the image'.format(n_targets))
      rand_targets = np.random.choice(self.targets, size=n_targets)
      imgs = [target['img'].copy() for target in rand_targets]

      # Apply random transforms and crops to targets
      imgs = self.random_transforms(imgs)

      new_targets = []
      for transformed_img, target in zip(imgs, rand_targets):
        new_target = {}
        new_target['img'] = transformed_img.copy()
        for key in target.keys() :
          if key is not 'img':
            new_target[key] = target[key]
        new_targets.append(new_target)


      out_targets = { 'height': self.h, 'width': self.w }

      img, target_positions = self.insert_portions(
               img, targets=new_targets, batches=None
             )

      out_targets['tlx'] = target_positions['tlx']
      out_targets['tly'] = target_positions['tly']
      out_targets['brx'] = target_positions['brx']
      out_targets['bry'] = target_positions['bry']
      out_targets['label'] = target_positions['label']

      return img, out_targets

  def random_transforms(self, targets):
      """Apply random transforms on the targets.
      """

      possible_mul_values = np.arange(7) * 0.2 + 0.4
      possible_mul_aug = [iaa.Multiply(value, True) for value in possible_mul_values]

      seq = iaa.Sequential([
        iaa.Crop(percent=(0., 0.1)),
        iaa.Fliplr(0.5),
        iaa.GaussianBlur(sigma=(0, 1.0)),
        iaa.Affine(rotate=(-179, 180)),
        iaa.OneOf(possible_mul_aug)
      ])
      return seq.augment_images(targets)

  def insert_portions(self, img, targets=None, batches=None):
      """Check for free position then insert targets and batches in the image.
      """

      # b_positions = []

      # ----------- This portion is going to be ignored -----------#
      # put the batches in the image
      # if batches is not None:
      #   for batch in batches :
      #     position = get_free_position(
      #             batch.shape,
      #             b_positions,
      #             size=size,
      #             override=True)

      #     if position is not None:
      #         img = insert_batch_in_img(batch, img, position)
      #         b_positions.append(position)
      # -----------------------------------------------------------#

      # define the output target positions dictionary
      output_positions = {
              'tlx': [],
              'tly': [],
              'brx': [],
              'bry': [],
              'label': []
            }
      t_positions = []

      # put the targets in the image
      for target in targets :
          position = self.get_free_position(
                  target['img'].shape,
                  t_positions,
                  override=False)

          if position is not None:
              img = self.insert_batch_in_img(target['img'], img, position)
              t_positions.append(position)
              output_positions['tlx'].append(int(position[0]))
              output_positions['tly'].append(int(position[1]))
              output_positions['brx'].append(int(position[2]))
              output_positions['bry'].append(int(position[3]))
              output_positions['label'].append(target['label'])
      return img, output_positions

